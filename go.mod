module pipoller

go 1.16

require (
	github.com/gin-gonic/gin v1.7.1
	github.com/rs/zerolog v1.21.0
	github.com/stretchr/testify v1.7.0
	go.uber.org/multierr v1.6.0 // indirect
	go.uber.org/zap v1.16.0 // indirect
)
