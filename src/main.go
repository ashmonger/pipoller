package main

import (
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

var startTime time.Time

func init() {
	startTime = time.Now()
}

func Uptime(ctx *gin.Context) {
	up := time.Since(startTime)
	ctx.JSON(
		http.StatusOK,
		gin.H{"uptime": up.Seconds()},
	)
}

func Hello(ctx *gin.Context) {
	ip := ctx.ClientIP()
	log.Printf("Client: %v", ip)
	ctx.JSON(
		http.StatusOK,
		gin.H{"hello": ip},
	)

}

func routerInit() *gin.Engine {
	router := gin.New()
	router.Use(gin.Recovery())
	router.GET("/hello", Hello)
	router.GET("/uptime", Uptime)

	//router.GET("/someGet", getting)
	//router.POST("/somePost", posting)
	return router
}

func main() {
	gin.SetMode(gin.ReleaseMode)
	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix

	r := routerInit()
	log.Print("Launching router on :8080")
	r.Run(":8080")
}
