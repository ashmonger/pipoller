build:
	go build -o bin/pipoller src/main.go

run:
	go run src/main.go

test:
	cd src; go test

linux:
	GOOS=linux GOARCH=amd64 go build -o bin/pipoller src/main.go

pi:
	GOOS=linux GOARCH=arm go build -o bin/pipoller-arm src/main.go

#arm64:
#	GOOS=linux GOARCH=arm64 go build -o bin/pipoller-arm64 src/main.go

clean:
	rm -f bin/pipoller*
